# undertow-custom-handler

Used to customise Undertow exception handling and logging.

## Usage

```
<dependency>
    <groupId>com.atlassian.product.fabric</groupId>
    <artifactId>undertow-custom-handler</artifactId>
    <version>1.0</version>
</dependency>
```

## Set exception swallowing resource manager

```java
private static class UndertowCustomiser implements EmbeddedServletContainerCustomizer, Ordered {

    @Override
    public void customize(final ConfigurableEmbeddedServletContainer container) {
        if (container instanceof UndertowEmbeddedServletContainerFactory) {
            UndertowEmbeddedServletContainerFactory undertowContainer = (UndertowEmbeddedServletContainerFactory) container;

            undertowContainer.addDeploymentInfoCustomizers(deploymentInfo -> {
                ResourceManager resourceManager = deploymentInfo.getResourceManager();
                deploymentInfo.setResourceManager(new ExceptionSwallowingDelegatingResourceManager(resourceManager));
            });
        }
    }

    @Override
    public int getOrder() {
        return 2;
    }
}

```