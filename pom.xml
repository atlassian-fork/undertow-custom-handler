<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <name>Library to introduce custom undertow error handling</name>
    <groupId>com.atlassian.product.fabric</groupId>
    <artifactId>undertow-custom-handler</artifactId>
    <version>1.5-SNAPSHOT</version>
    <packaging>jar</packaging>

    <prerequisites>
        <maven>3.3.9</maven>
    </prerequisites>
    <properties>
        <java.version>8</java.version>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>

        <!-- Plugins -->
        <maven.compiler.plugin.version>3.6.1</maven.compiler.plugin.version>
        <error.prone.version>2.0.12</error.prone.version>

        <!--undertow.version is used to override Spring Boot Undertow Starter 1.5.x dependency to Undertow which is
        affected by the vulnerability CVE-2017-7559 (https://goo.gl/tMb1ED). It was fixed in 1.4.23.
        Can be removed when upgrading to Spring Boot 1.15.11 or 2.x
        (https://github.com/spring-projects/spring-boot/issues/12384)-->
        <undertow.version>1.4.23.Final</undertow.version>
        <sl4j.version>1.7.25</sl4j.version>

        <!-- Tests properties -->
        <mockito.version>2.7.20</mockito.version>
        <junit.version>4.12</junit.version>
    </properties>

    <scm>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/undertow-custom-handler.git</developerConnection>
        <url>https://bitbucket.org/atlassian/undertow-custom-handler</url>
        <tag>undertow-custom-handler-1.1</tag>
    </scm>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>private-pom</artifactId>
        <version>5.0.8</version>
    </parent>

    <dependencies>
        <!--Remove direct dependency to undertow when upgrading to Spring Boot 1.5.11 or 2.x-->
        <dependency>
            <groupId>io.undertow</groupId>
            <artifactId>undertow-core</artifactId>
            <version>${undertow.version}</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${sl4j.version}</version>
        </dependency>

        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.2.3</version>
        </dependency>

        <!-- Test -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
            <version>${junit.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <compilerId>javac-with-errorprone</compilerId>
                    <forceJavacCompilerUse>true</forceJavacCompilerUse>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.compiler.target}</target>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.codehaus.plexus</groupId>
                        <artifactId>plexus-compiler-javac-errorprone</artifactId>
                        <version>2.8</version>
                    </dependency>
                    <!-- override plexus-compiler-javac-errorprone's dependency on Error Prone with the latest version -->
                    <dependency>
                        <groupId>com.google.errorprone</groupId>
                        <artifactId>error_prone_core</artifactId>
                        <version>${error.prone.version}</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                  <systemPropertyVariables>
                    <log4j.configuration>log4j.xml</log4j.configuration>
                  </systemPropertyVariables>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                  <additionalparam>-Xdoclint:none</additionalparam>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
