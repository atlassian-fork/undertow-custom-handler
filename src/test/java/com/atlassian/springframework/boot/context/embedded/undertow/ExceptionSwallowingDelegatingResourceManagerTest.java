package com.atlassian.springframework.boot.context.embedded.undertow;

import io.undertow.ExceptionSwallowingDelegatingResourceManager;
import io.undertow.server.handlers.resource.ResourceManager;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class ExceptionSwallowingDelegatingResourceManagerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    
    @Mock
    ResourceManager mockResourceManager;
    
    @Test
    public void testCatchingIllegalArgumentException() throws Exception {
        final String resourceName = "monkeytrousers";
        when(mockResourceManager.getResource(resourceName)).thenThrow(IllegalArgumentException.class);
        
        try (ResourceManager delegatingResourceManager = new ExceptionSwallowingDelegatingResourceManager(mockResourceManager)) {
            assertNull(delegatingResourceManager.getResource(resourceName));
        }
    }
}
