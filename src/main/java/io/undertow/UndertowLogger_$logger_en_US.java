package io.undertow;

import org.jboss.logging.Logger;

@SuppressWarnings("unused")
public class UndertowLogger_$logger_en_US extends UndertowLogger_$logger {
    public UndertowLogger_$logger_en_US(final Logger log) {
        super(log);
    }

    protected String undertowRequestFailed$str() {
        return "UT005071: Undertow request failed";
    }
}