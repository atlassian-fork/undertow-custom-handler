package io.undertow;

import io.undertow.server.handlers.resource.Resource;
import io.undertow.server.handlers.resource.ResourceChangeListener;
import io.undertow.server.handlers.resource.ResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

/**
 * A delegating ResourceManager that will catch any exceptions coming from the delegate when retrieving
 * a resource and swallow it and return null instead, indicating a resource did not exist.
 * <br>
 * This deals with the problems observed in:
 * <ul>
 *  <li>https://product-fabric.atlassian.net/browse/FS-1818
 *  <li>https://product-fabric.atlassian.net/browse/FS-1828
 * </ul>
 */
@SuppressWarnings("unused")
public class ExceptionSwallowingDelegatingResourceManager implements ResourceManager {

    private static final Logger log = LoggerFactory.getLogger(ExceptionSwallowingDelegatingResourceManager.class);
    
    private final ResourceManager delegateResourceManager;
    
    public ExceptionSwallowingDelegatingResourceManager(ResourceManager delegate) {
        this.delegateResourceManager = requireNonNull(delegate);
    }

    @Override
    public void close() throws IOException {
        delegateResourceManager.close();
    }

    @Override
    public Resource getResource(String path) throws IOException {
        try {
            return delegateResourceManager.getResource(path);
        } catch (IllegalArgumentException e) {
            if (log.isInfoEnabled()) {
                log.info("IllegalArgumentException for getResource(\"" + path + "\")", e);
            }
            return null;
        }
    }

    @Override
    public boolean isResourceChangeListenerSupported() {
        return delegateResourceManager.isResourceChangeListenerSupported();
    }

    @Override
    public void registerResourceChangeListener(ResourceChangeListener listener) {
        delegateResourceManager.registerResourceChangeListener(listener);
    }

    @Override
    public void removeResourceChangeListener(ResourceChangeListener listener) {
        delegateResourceManager.removeResourceChangeListener(listener);
    }
}
