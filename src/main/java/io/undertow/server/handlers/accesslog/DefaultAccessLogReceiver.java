package io.undertow.server.handlers.accesslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.concurrent.Executor;

/**
 * Hacky way to get Undertow logs mixed with regular logs instead of writing them to a separate file
 */
@SuppressWarnings("unused")
public class DefaultAccessLogReceiver implements AccessLogReceiver {

    private static final Logger log = LoggerFactory.getLogger(DefaultAccessLogReceiver.class);

    @SuppressWarnings({"UnusedParameters", "unused"})
    public DefaultAccessLogReceiver(final Executor logWriteExecutor, final File outputDirectory, final String logBaseName, final String logNameSuffix) {

    }

    @SuppressWarnings({"UnusedParameters", "unused"})
    public DefaultAccessLogReceiver(final Executor logWriteExecutor, final File outputDirectory, final String logBaseName, final String logNameSuffix, boolean rotate) {

    }

    public void logMessage(final String message) {
        log.info(message);
    }
}
